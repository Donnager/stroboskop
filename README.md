# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://Donnager@bitbucket.org/Donnager/stroboskop.git .

```

Naloga 6.2.3:
https://bitbucket.org/Donnager/stroboskop/commits/69ec2bad08d6d12f0374039134d9a3d17c548460?at=master

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/Donnager/stroboskop/commits/22c3e9d69d7b03ea01374f75bd9a62eb7046e20b?at=izgled

Naloga 6.3.2:
https://bitbucket.org/Donnager/stroboskop/commits/8f0781ace5496ea586aed2545e80cee365c21bf0?at=izgled

Naloga 6.3.3:
https://bitbucket.org/Donnager/stroboskop/commits/a4b39e3b72efd1f65c4bbe97d39cd3261ace1ece?at=izgled

Naloga 6.3.4:
https://bitbucket.org/Donnager/stroboskop/commits/cbf7f7c2f93197924aa1e4c8fe585dd966cc4b1c?at=izgled

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/Donnager/stroboskop/commits/860ed098fa9ed089f2b77b871e13891ae837b632

Naloga 6.4.2:
https://bitbucket.org/Donnager/stroboskop/commits/4c0fd50171cf320574cabac6e5d4ea95c8f035c9

Naloga 6.4.3:
https://bitbucket.org/Donnager/stroboskop/commits/eb5edbdc660a31eddec91e33542a3b5aa5ce5359

Naloga 6.4.4:
https://bitbucket.org/Donnager/stroboskop/commits/e351ce95da479973c0ba6f911f46ecb3c7987259